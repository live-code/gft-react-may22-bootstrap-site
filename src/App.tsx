import React from 'react';
import logo from './logo.svg';
import './App.css';
import { PricingPage } from './pages/pricing/PricingPage';
import { NavBar } from './core/NavBar';
import { Footer } from './core/Footer';
import { HomePage } from './pages/home/HomePage';
import { ContactsPage } from './pages/contacts/ContactsPage';
import { BrowserRouter, Link, Navigate, Route, Routes } from 'react-router-dom';
import { CatalogPage } from './pages/catalog/CatalogPage';
import { CatalogDetails } from './pages/catalog/CatalogDetails';
import create from 'zustand';

type AppState = {
  counter: number;
  theme: 'dark' | 'light';
  inc: () => void;
  reset: () => void;
  changeTheme: (val: 'dark' | 'light') => void;
}

export const store = create<AppState>(set => ({
  counter: 10,
  theme: 'dark',
  inc: () => set(state => ({ counter: state.counter + 1 })),
  reset: () => set({ counter: 0 }),
  changeTheme: val => set({ theme: val })
}))


function App() {
  console.log('render')
  return (
    <BrowserRouter>
      <div>

        <div className="container py-3">
          <NavBar />
          <Routes>
            <Route path="homepage" element={<HomePage />} />
            <Route path="contacts" element={<ContactsPage />} />
            <Route path="pricing" element={<PricingPage />} />
            <Route path="catalog" element={<CatalogPage />} />
            <Route path="catalog/:userId" element={<CatalogDetails />} />
            <Route path="/" element={
              <Navigate to="homepage" />
            }/>

            <Route path="*"
                   element={
                      <div>
                        <p>This page does not exist</p>
                        <Link to="/">Go to Home</Link>
                      </div>
                   }
            />
          </Routes>
          <Footer />
        </div>
      </div>

    </BrowserRouter>
  );
}

export default App;



store.subscribe(state => {
  console.log('---->', state)
})
