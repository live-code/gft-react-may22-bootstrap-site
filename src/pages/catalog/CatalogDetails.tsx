import { Link, useNavigate, useParams } from 'react-router-dom';
import { useEffect, useState } from 'react';
import axios from 'axios';

export function CatalogDetails() {
  const params = useParams();
  const navigate = useNavigate();
  const [user, setUser] = useState<any | null>({  name: '', phone: '' })

  useEffect(() => {
    axios.get(`https://jsonplaceholder.typicode.com/users/${params.userId}`)
      .then(res => setUser(res.data))
  }, [])

  function onChangeHandler(e: React.ChangeEvent<HTMLInputElement>) {
    setUser({ ...user, [e.target.name]: e.target.value})
  }

  function save() {
    axios.patch(`https://jsonplaceholder.typicode.com/users/${params.userId}`, user)
      .then(res => {
        alert('aggiornato')
      })

  }
  function deleteUser() {
    axios.delete(`https://jsonplaceholder.typicode.com/users/${params.userId}`)
      .then(res => {
        navigate('/catalog')
      })

  }

  return <div>
    <input type="text" value={user?.name} onChange={onChangeHandler} name="name"/>
    <input type="text" value={user?.phone} onChange={onChangeHandler} name="phone"/>
    <button onClick={deleteUser}>DELETE</button>
    <button onClick={save}>SAVE</button>

    <p>{user?.phone}</p>
    <p>{user?.website}</p>

    <hr/>
    <Link to='/catalog'>Back to list</Link>
  </div>
}
