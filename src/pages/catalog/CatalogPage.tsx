import { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';

export function CatalogPage() {
  const [users, setUsers] = useState<any[]>([])

  useEffect(() => {
    axios.get<any[]>('https://jsonplaceholder.typicode.com/users')
      .then(res => setUsers(res.data))
  }, [])
  return  <div>
    <h1>User List</h1>
    {
      users.map(user => {
        return <li key={user.id}>
          <Link to={'/catalog/' + user.id}>
            {user.name}
          </Link>
        </li>
      })
    }
  </div>
}
