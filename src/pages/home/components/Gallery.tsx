import React, { useContext } from 'react';
import { HomeContext } from '../HomePage';

export const Gallery = React.memo(() => {
  const state = useContext(HomeContext)
  console.log('render gallery', state?.gallery)
  return <div>
    <h1>Gallery</h1>
    {
      state?.gallery.map(item => <li key={item}>{item}</li>)
    }
  </div>
})
