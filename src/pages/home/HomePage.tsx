import React, { useContext, useEffect, useState } from 'react';
import { Gallery } from './components/Gallery';

interface MyData {
  news: string[],
  gallery: number[]
}

export const HomeContext = React.createContext<MyData | null>(null)

export function HomePage() {
  const [data, setData] = useState<MyData>({ news: [], gallery: []})
  const [count, setCount] = useState<number>(0)

  useEffect(() => {
    setTimeout(() => {
      // axios.get...
      const res: MyData = {
        news: ['news1', 'news2', 'news3'],
        gallery: [10, 20, 30]
      }
      setData(res)
    }, 1000)
  }, [])

  function updateNews() {
    setData({
      ...data,
      news: ['title2']
    })
  }

  return <div>
    <HomeContext.Provider value={data}>
      <button onClick={updateNews}>update news</button>
      <button onClick={() => setCount(c => c+ 1)}>+ {count}</button>
      <hr/>
      <News data={data.news} />
      <Gallery />
    </HomeContext.Provider>
  </div>
}


// ----------------
interface NewsProps {
  data: string[]
}
export const News = React.memo((props: NewsProps) => {
  console.log('render news')
  return <div>
    <h1>News</h1>
    {
      props.data.map(item => <li key={item}>{item}</li>)
    }
  </div>
})
