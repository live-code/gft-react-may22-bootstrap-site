import React from 'react';

interface PricingTableProps {
  data: any[];
}

export function PricingTable(props: PricingTableProps) {
  return (
    <>
      <h2 className="display-6 text-center mb-4">Compare plans</h2>
      <div className="row row-cols-1 row-cols-md-3 mb-3 text-center">
        {
          props.data.map(price => {
            return (
              <div className="col" key={price.name}>
                <div className="card mb-4 rounded-3 shadow-sm">
                  <div className="card-header py-3">
                    <h4 className="my-0 fw-normal">{price.name}</h4>
                  </div>
                  <div className="card-body">
                    <h1 className="card-title pricing-card-title">${price.cost}<small className="text-muted fw-light">/mo</small></h1>
                    <ul className="list-unstyled mt-3 mb-4">
                      <li>{price.users} users included</li>
                      <li>{price.storage} GB of storage</li>
                      <li>{price.support}</li>
                      <li>Help center access</li>
                    </ul>
                    <a href={price.linkBtn} target="_blank" type="button" className="w-100 btn btn-lg btn-outline-primary">
                      {price.labelBtn}
                    </a>
                  </div>
                </div>
              </div>
            )
          })
        }
      </div>
    </>
  )
}
