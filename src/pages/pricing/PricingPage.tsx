import React from 'react';
import { PricingTable } from './components/PricingTable';

const pricing = [
  {
    name: 'Free',
    cost: 0,
    users: 10,
    storage: 2,
    support: 'Email support',
    labelBtn: 'Sign Up for Free',
    linkBtn: 'http://www.google.com'
  },
  {
    name: 'Pro',
    cost: 15,
    users: 20,
    storage: 5,
    support: 'Priority Email support',
    labelBtn: 'Upgrade',
    linkBtn: 'http://www.facebook.com'
  },
  {
    name: 'Enterprise',
    cost: 29,
    users: 30,
    storage: 10,
    support: 'Phone support',
    labelBtn: 'Upgrade',
    linkBtn: 'http://www.google.com'
  }
]

export function PricingPage() {
  return (
    <main>
      { /* CheckBox Icon */ }
      <svg xmlns="http://www.w3.org/2000/svg" style={{display: 'none'}}>
        <symbol id="check" viewBox="0 0 16 16">
          <title>Check</title>
          <path d="M13.854 3.646a.5.5 0 0 1 0 .708l-7 7a.5.5 0 0 1-.708 0l-3.5-3.5a.5.5 0 1 1 .708-.708L6.5 10.293l6.646-6.647a.5.5 0 0 1 .708 0z" />
        </symbol>
      </svg>

      {/*Welcome msg*/}
      <div className="pricing-header p-3 pb-md-4 mx-auto text-center">
        <h1 className="display-4 fw-normal">Pricing</h1>
        <p className="fs-5 text-muted">Quickly build an effective pricing table for your potential customers with this Bootstrap example. It’s built with default Bootstrap components and utilities with little customization.</p>
      </div>

      <PricingTable data={pricing} />

      {/*Plans*/}
      <div className="table-responsive">
        <table className="table text-center">
          <thead>
          <tr>
            <th style={{width: '34%'}} />
            <th style={{width: '22%'}}>Free</th>
            <th style={{width: '22%'}}>Pro</th>
            <th style={{width: '22%'}}>Enterprise</th>
          </tr>
          </thead>
          <tbody>
          <tr>
            <th scope="row" className="text-start">Public</th>
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
          </tr>
          <tr>
            <th scope="row" className="text-start">Private</th>
            <td />
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
          </tr>
          </tbody>
          <tbody>
          <tr>
            <th scope="row" className="text-start">Permissions</th>
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
          </tr>
          <tr>
            <th scope="row" className="text-start">Sharing</th>
            <td />
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
          </tr>
          <tr>
            <th scope="row" className="text-start">Unlimited members</th>
            <td />
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
          </tr>
          <tr>
            <th scope="row" className="text-start">Extra security</th>
            <td />
            <td />
            <td><svg className="bi" width={24} height={24}><use xlinkHref="#check" /></svg></td>
          </tr>
          </tbody>
        </table>
      </div>
    </main>
  )
}
